# te

## Description
This is a repost of a VERY old script for an old chat engine [epic4](http://www.epicsol.org/start) engine that I liked to use back in the day, instructions on installing and use are in process and will be posted as soon as I have it back up and running.

## Status
Currently archive only as I'm not sure how to maintain this chat client currently. All bugs or issues will be ignored.

## Installation
1. Clone the project (download the tgz file, unzip the file with ```tar xpvf te-latest.tgz```)
2. Move the ```.te``` directory into your home directory
3. Move the ```.ircrc``` file into your home directory
4. Install epic4 on the server, or ask that the admin do that for you.
    for debian
    ```sudo apt install epic4```

5. Launch irc client with ```irc```
6. Wait for a connection
7. use ```/servar <server address>``` to connect to your favorite server.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
None

## Roadmap
Project archive only at this time.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.
